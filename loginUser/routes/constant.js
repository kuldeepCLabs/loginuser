/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "INVALID_ACCESS_TOKEN", 101);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 102);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);
define(exports.responseStatus, "SHOW_MESSAGE", 104);
define(exports.responseStatus, "SHOW_DATA", 105);

exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN", "Please logout again, Invalid access.");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "Successfully login the user");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "INVALID_EMAIL", "You ENTER WRONG EMAIL");
define(exports.responseMessage, "INVALID_USER", "this is invalid username");
define(exports.responseMessage, "UPDATE_USER", "Successfully update the user");
define(exports.responseMessage, "LOGOUT_USER", "Successfully logout the user");
define(exports.responseMessage, "INVAILD_PASSWORD", "enter invaild password. please try again");
define(exports.responseMessage, "CHANGE_PASSWORD", "Successfully change  the password");


exports.deviceType = {};
define(exports.deviceType, "ANDROID",   0);
define(exports.deviceType, "iOS",       1);