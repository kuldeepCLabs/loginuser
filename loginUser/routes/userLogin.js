var express = require('express');
var router = express.Router();
var func=require('./commonFunction');
var sendResponse=require('./sendResponse');
var async=require('async');
var md5 = require('MD5');
var request=require('request');

/* GET users listing. */


//Signup user
router.post('/signup_user',function(req,res){
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var deviceType = req.body.device_type; // 1 for android, 2 for ios
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var manValues=[firstName,lastName,email,password,deviceType,deviceToken,appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            checkEmailAvailability(res, email, callback);
        }
    ],function(err,result){

        if(err){
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var curLoginTime = new Date();
            var accessToken = func.encrypt(email + curLoginTime);
            var encryptPassword = md5(password);
             console.log("insert statement");
            var sql = "INSERT INTO `userlogin`(`first_name`, `last_name`, `email`, `password`,`access_token`,`device_type`,`device_token`,`app_version`,`cur_login_time`) VALUES (?,?,?,?,?,?,?,?,?)";
            var values = [firstName, lastName, email, encryptPassword, accessToken, deviceType, deviceToken, appVersion, curLoginTime];

            dbConnection.Query(res, sql, values, function (result) {

                var data = {
                    access_token: accessToken,
                    first_name: firstName,
                    last_name: lastName
                };
                func.sendMail(email,firstName,res,function(callback){
                    if(callback==1)
                sendResponse.sendSuccessData(data, res);
                });
            });
        }
    });

});
//login user
router.post('/login_user',function(req,res) {
    var email = req.body.email;
    var pass = req.body.password;
    var deviceType = req.body.device_type; // 1 for android, 2 for ios
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    var manValues = [email, pass,deviceType,deviceToken,appVersion];
    async.waterfall([
        function (callback) {
            func.checkBlank(res, manValues, callback);
        }
    ], function (err, resu) {
        if(err){
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var values = [email];
            var sql = 'select `password`,`user_id`,`device_type`,`device_token`,app_version from `userlogin` where email=? limit 1';

            dbConnection.Query(res, sql, values, function (resultEmail) {

                var emailLength = resultEmail.length;
                 console.log(resultEmail.length);
                if ( emailLength == 0) {
                    sendResponse.sendErrorMessage(constant.responseMessage.INVALID_USER,res);
                }
                else {
                    var password1 = resultEmail[0].password;
                    var encrptPassword = md5(pass);
                    console.log(password1==encrptPassword);
                    if (password1 == encrptPassword) {

                        var id = resultEmail[0].user_id;
                        var curLoginTime = new Date();
                        var accessToken = func.encrypt(email + curLoginTime);
                       // console.log(id);
                        sql1 = 'update `userlogin` set user_status=?,access_token=? where user_id=?';
                        values = [1,accessToken,id];
                        dbConnection.Query(res, sql1, values, function (result) {
                            console.log(result+"define the result");
                            if (result.length !=0) {
                                 console.log("succesfully update the user login table with new access token");
                                data={
                                    new_access_token:accessToken,
                                    email:email
                                };
                                sendResponse.sendSuccessData(data,res);
                            }
                            else {
                                sendResponse.somethingWentWrongError(res);
                            }
                        });
                    }
                    else {
                        sendResponse.sendErrorMessage(constant.responseMessage.INVAILD_PASSWORD, res);
                    }
                }
            });
        }
    });
});
//logout function
router.post('/logout',function(req,res) {
    var accessToken = req.body.access_token;
    console.log(accessToken);
    var checkBlank = [accessToken];
    func.checkBlank(res, checkBlank, function (callback) {
        if (callback == null) {
            func.authenticateUser(res,accessToken, function (result) {
                var user_id = result[0].user_id;
                var sql = "UPDATE `userlogin` SET `user_status`=?,`device_token`=? WHERE `user_id`=? LIMIT 1";   //user status 1 for login and 0 for logout
                dbConnection.Query(res,sql, [0, "", user_id], function (result_update) {
                   console.log("logout update the status");
                    if(result_update.length!=0) {
                        sendResponse.successStatusMsg(constant.responseMessage.LOGOUT_USER, res);
                    }
                    else{
                        sendResponse.somethingWentWrongError(res);
                    }
                });
            });
        }
    });
});


//that function is check the email already exists or not
function checkEmailAvailability(res, email, callback){

    var sql='select `user_id` from `userlogin` where `email`=? limit 1';
    var values=[email];
  //console.log("email check");
    dbConnection.Query(res,sql,values,function(userResponse){

        console.log(userResponse.length+"jddsd");
       if(userResponse.length){
           console.log("already exists");
           sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_EXISTS,res);
       }
        else{
           checkEmailValidation(res,email,callback);
           if(callback==null) {
               console.log("not valid");
               callback(null);
           }
       }
    });

}
//check the email domain name is valid or not
function checkEmailValidation(ress,email,callback){

    request({
        url: "https://www.emailitin.com/email_validator",
        method: "POST",
        form: {email: email},
        json: true
    }, function (err,res,body) {
        console.log(body.valid);
        if (body.valid) {
            callback(null);
        }
        else {
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_EMAIL,ress);
        }

    });
}

module.exports = router;
