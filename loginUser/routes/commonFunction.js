/**
 * Created by GARG on 3/1/2015.
 */
var sendResponse=require('./sendResponse');
var autht=config.get('emailSettings');
var nodemailer = require('nodemailer');


//check the blank value

exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == ''||arr[i] == undefined||arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}
//encrypt the password through the crypto
exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}
//check the user is access token valid or inbvalid
exports.authenticateUser = function(res,userAccessToken, callback)
{
    var sql = "SELECT `user_id` FROM `userlogin` WHERE `access_token`=? LIMIT 1";
    dbConnection.Query(res,sql, [userAccessToken], function(result) {
           console.log(result.length);
        if (result.length > 0) {
            return callback(result);
        } else {
            sendResponse.invalidAccessTokenError(res);
        }
    });
};
//send the mail
exports.sendMail=function(email,name,res,callback) {


     var smtpTransport = nodemailer.createTransport("Direct", {debug: true}

     );

    var mailOptions = {
        from: autht.emailFrom,
        to:email,
        subject: "Successfully Registered",
        text: "Check the email for testing"+name
    }

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            sendResponse.somethingWentWrongError(res);
        } else {
              return callback(1);
        }
    });

}
